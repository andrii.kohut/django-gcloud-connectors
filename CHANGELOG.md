Changelog
=========

## Development Version (targeting 1.3.0)

## 1.2.0

 - Google Cloud Firestore is now a supported backend
 - `gcloudc.db.datastore.transaction` has been removed in favour of supporting Django's normal transactions
 - Entirely refactored database connector to support multiple databases
 - Improvements and simplifications of the caching layer
 - Added utility functions for Firestore index management
 - Added support for Django 4.2

## 1.1.0
Note: this is the last minor release which contains custom handling of transactions.
If your code uses `gcloudc.db.datastore.transaction.atomic` you should use this version.

### Breaking Changes

- Dropped support for django versions < 3.2

### New features & improvements

- Add support to exclude fields from the datastore index using Meta.indexes. See the
field_indexes documentation for more details.
- Add support for Django 4.0 and 4.1. Note: support with Django 4.2 is not yet provided

### Bug fixes

- Fix an bug where repeating certain queries that use a projection could return inconsistent results (see #56)

## 1.0.0

### Breaking Changes
- Dropped support for the "old" datastore and the `OPTIMISTIC_WITH_ENTITY_GROUPS` (legacy) concurrency
mode for firestore in datastore mode. From now on only Firestore in Datastore mode with either
`OPTIMISTIC` or `PESSIMISTIC` concurrency mode is supported.
- Transaction now work properly. While this is "formally" a bug fix, if you were using
`transaction.atomic()` with a previous version of the connector, chances are your code may stop
working since transaction are now working as intended.

### New features & improvements
- Test can now be run on a real datastore instance (see contributing.md)

### Bug fixes
- Fix a bug were transactions wouldn't behave correctly because of [a bug](https://github.com/googleapis/python-datastore/issues/447)
in the underlying python datastore SDK which doesn't honour transaction when the transaction is
managed with `begin()` and `commit()/rollback()`
- Fix a bug where unique constraint would stop working inside an atomic block
- Fix a bug where the cache in an atomic block wouldn't work unless the model had unique constraints (other than the PK - see #44)
- Fix bug where `in_atomic_block` would not work as expected on non default connections.


## v0.4.0
Note: this is likely the last release compatible with the original Datastore and Cloud Firestore
in Datastore mode with `OPTIMISTIC_WITH_ENTITY_GROUPS` concurrency mode. Future version will
only support Cloud Firestore in Datastore mode with either `PESSIMISTIC` (the default) or
`OPTIMISTIC` concurrency mode.

You can read about concurrency modes in the [official docs](https://cloud.google.com/datastore/docs/concepts/transactions#concurrency_modes).

The 0.X.Y versions will be reserved for backward compatible critical fixes.

### New features & improvements
- Added support for running tests on real firestore in datastore mode instance

### Bug fixes
- Added support for `PositiveBigIntegerField`
- Fix handling of null values in unique_together

## v0.3.7

### New features & improvements

- Added support for using multiple values with `__contains` filters on iterable fields.

### Bug fixes

- Fix a bug where custom fields widget wouldn't work due to extra "renderer" param in the render method (introduced in Django 1.11)
- Fix a bug where querying on a decimal field wouldn't work

## v0.3.6

- N/A (changelog didn't exist before this version)
