Transactions
============

Transactions on the Cloud Datastore behave differently to those of a SQL database.
It's worth reading the [Cloud Datastore transactions documentation](https://cloud.google.com/datastore/docs/concepts/transactions)
to familiarise yourself with their behaviour.
Crucially:

> "queries and lookups inside a Datastore mode transaction do _not_ see the results of previous writes inside that transaction".

This is partially mitigated by our caching system. For more info on how querying and caching work see the [caching](../caching) docs.

